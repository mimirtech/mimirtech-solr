name		 "mimirtech-solr"
maintainer       "Mimirtech"
maintainer_email "adam@hipsnip.com/remy@hipsnip.com"
license          "Apache 2.0"
description      "Installs/Configures Solr"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
supports 		 'ubuntu', ">= 12.04"
version          "0.1.0"

depends "hipsnip-solr", "~> 0.5.0"
